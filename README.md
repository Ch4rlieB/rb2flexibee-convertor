# rb2flexibee-convertor
Convertor of Raiffeisenbank statements in **CSV file** to FlexiBee XML which can be imported directly to ABRA FlexiBee.

Based on [Convertor by Massimo Filippi](https://github.com/massimo-filippi/flexibee-tools). Thanks :-)

## Configuration
This script doesn't require any configuration or changing. All settings can be done by parameters.

## Usage
```shell
php rb2fb.php [-options] [-i file] [-o file] [-a bank_account_code]
```

### Examples
```shell
php rb2fb.php
```
Converts statements in file named *input.csv*. Output in FlexiBee XML format is saved to file named *output.xml*. Both filenames are default. 
As bank account code is selected default value *BANKOVNÍ ÚČET*. This is the default bank account in every new FlexiBee company.


```shell
php rb2fb.php -i my_csv_file_name.csv -o my_flexibee_xml_file_name.xml -a 'RB-CZK'
```
Converts statements in file named *my_csv_file_name.csv* and output is saved to file *my_flexibee_xml_file_name.xml*. 
With parameters **-i** and **-o** can be specified input and output file names.
As bank account is selected one with code *RB-CZK*. Bank account with this code must be created before importing of output file to FlexiBee.

```shell
php rb2fb.php -h
```
Shows help.

### Complete parameters list
* **-i** specify input file, CSV file downloaded from eKonka. Default = input.csv
* **-o** specify output file, XML suitable for importing into FlexiBee. Default = output.xml
* **-a** specify bank account code from FlexiBee. Default = BANKOVNÍ ÚČET
* **-h** shows help
* **-d** send output to stdout instead of file (if used, there will be no sucess info in output)
* **-x** disable writing to output file
