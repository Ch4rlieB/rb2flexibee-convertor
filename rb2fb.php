#!/usr/bin/php
<?php
/*

Tool for converting Raiffeisenbank eKonto payments to FlexiBee XML
Author: Maxim Krušina, maxim@mfcc.cz, Massimo Filippi, s.r.o.
Homepage: https://github.com/massimo-filippi/flexibee-tools

Notes:
Source encoding: Windows 1250
Destinantion encoding: UTF-8

Other resources:
http://www.abclinuxu.cz/blog/doli/2012/4/konverze-bankovnich-vypisu-rb-do-formatu-abo

ToDo:
- Add UI - ie https://www.sveinbjorn.org/platypus
- Remove first empty line in output file

Usage:
======
- Set all parameters in section bellow (User specific settings)
- Export file from RB / eKonto via: Historie účtu / Pohyby na účtu / Formát CSV (button under the listing)
- Rename CSV file to input.csv and put it into same solder as this script
- Run: ./rb2fb.php > output.xml
- Import
-- Menu: Nástroje / Import / Import z XML

*/

// Include values from config file, if exists
if(file_exists('config.php'))
    include 'config.php';

// Options parsing
$shortopts  = "";
$shortopts .= "d";       // Display output
$shortopts .= "h";       // Help, do not accept values
$shortopts .= "i:";      // Input file, value required
$shortopts .= "o:";      // Output file, value required
$shortopts .= "x";       // Disable writing to output file
$shortopts .= "a:";      // Bank account code

$options = getopt($shortopts);
//var_dump($options);

// Write Help
if (array_key_exists("h", $options)) {
    echo "Use ./" . basename(__FILE__) . " -h for help\n\n";
    echo "rb2fb\n";
    echo "=====\n";
    echo "\n";
    echo "This tool will convert Raiffeisenbank eKonto payments (exportable as CVS) to FlexiBee XML file format, which can be imported.\n";
    echo "Without this tool, you're forced to install and use Raiffeisenbank's tool eKomunikátor, which is both pricey and very cumbersome.\n";
    echo "Homepage: https://gitlab.com/Ch4rlieB/rb2flexibee-convertor\n";
    echo "More info about FlexiBee: https://www.flexibee.eu/\n";
    echo "\n";
    echo "Usage\n";
    echo "-----\n";
    echo "./rb2fb.php [-options] [-i file] [-o file]\n";
    echo "-d                      send output to stdout instead of file (if used, there will be no sucess info in output)\n";
    echo "-h                      show this help\n";
    echo "-i                      specify input file, CSV file downloaded from Raiffeisenbank. Default = input.csv\n";
    echo "-o                      specify output file, XML suitable for importing into FlexiBee. Default = output.xml\n";
    echo "-a                      specify bank account code from FlexiBee. Default = BANKOVNÍ ÚČET";
    echo "-x                      disable writing to output file\n";
    echo "\n";

    exit();
}

// Deine constants (CSV offsets)
const DATUM             = 0;
const DATUM_ODEPSANI    = 1;
const CISLO_UCTU        = 5;
const NAZEV_UCTU        = 6;
const ZPRAVA            = 7;
const POZNAMKA          = 8;
const VARIABILNI_SYMBOL = 10;
const KONSTANTNI_SYMBOL = 11;
const SPECIFICKY_SYMBOL = 12;
const CASTKA            = 13;
const SMENA             = 14;
const KOD_TRANSAKCE     = 18;

// unused
const VALUTA            = 6;
const TYP               = 7;
const POPLATEK          = 13;
const CAS               = 1;

// Convert Win 1250 > UTF-8
function w1250_to_utf8($text) {
    // map based on:
    // http://konfiguracja.c0.pl/iso02vscp1250en.html
    // http://konfiguracja.c0.pl/webpl/index_en.html#examp
    // http://www.htmlentities.com/html/entities/
    $map = array(
        chr(0x8A) => chr(0xA9),
        chr(0x8C) => chr(0xA6),
        chr(0x8D) => chr(0xAB),
        chr(0x8E) => chr(0xAE),
        chr(0x8F) => chr(0xAC),
        chr(0x9C) => chr(0xB6),
        chr(0x9D) => chr(0xBB),
        chr(0xA1) => chr(0xB7),
        chr(0xA5) => chr(0xA1),
        chr(0xBC) => chr(0xA5),
        chr(0x9F) => chr(0xBC),
        chr(0xB9) => chr(0xB1),
        chr(0x9A) => chr(0xB9),
        chr(0xBE) => chr(0xB5),
        chr(0x9E) => chr(0xBE),
        chr(0x80) => '&euro;',
        chr(0x82) => '&sbquo;',
        chr(0x84) => '&bdquo;',
        chr(0x85) => '&hellip;',
        chr(0x86) => '&dagger;',
        chr(0x87) => '&Dagger;',
        chr(0x89) => '&permil;',
        chr(0x8B) => '&lsaquo;',
        chr(0x91) => '&lsquo;',
        chr(0x92) => '&rsquo;',
        chr(0x93) => '&ldquo;',
        chr(0x94) => '&rdquo;',
        chr(0x95) => '&bull;',
        chr(0x96) => '&ndash;',
        chr(0x97) => '&mdash;',
        chr(0x99) => '&trade;',
        chr(0x9B) => '&rsquo;',
        chr(0xA6) => '&brvbar;',
        chr(0xA9) => '&copy;',
        chr(0xAB) => '&laquo;',
        chr(0xAE) => '&reg;',
        chr(0xB1) => '&plusmn;',
        chr(0xB5) => '&micro;',
        chr(0xB6) => '&para;',
        chr(0xB7) => '&middot;',
        chr(0xBB) => '&raquo;',
    );
    return html_entity_decode(mb_convert_encoding(strtr($text, $map), 'UTF-8', 'ISO-8859-2'), ENT_QUOTES, 'UTF-8');
}

// Main code

// Set input file
$filename_input = "input.csv";
if (array_key_exists("i", $options)) {
    // Use file specifiled in command parameter -i
    $filename_input = $options["i"];
}

// Set output file
$filename_output = "output.xml";
if (array_key_exists("o", $options)) {
    // Use file specifiled in command parameter -o
    $filename_output = $options["o"];
}

// Code of bank account in FlexiBee
$BankAccCode = "BANKOVNÍ ÚČET";
if (array_key_exists("a", $options)) {
    // Use code specifiled in command parameter -a
    $BankAccCode = $options["a"];
}


if (($handle = @fopen($filename_input, "r")) == FALSE) {
    // Handle file open error
    echo "Error, cannot open input file: " . $filename_input . "\n";
    echo "Use ./" . basename(__FILE__) . " -d for help\n\n";
    exit();
} else {
    $out  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    $out .= "<winstrom version='1.0'>\n";
    // skip the first line of csv (header row)
    $data = fgetcsv($handle, 1000, ";");
    for ($row = 1; ($data = fgetcsv($handle, 1000, ";")) !== FALSE; $row++) {
        // Convert Win1250 to UTF 8
        foreach ($data as &$text) {
            $text = w1250_to_utf8($text);
        }
        // Date string refarmating
        $date = date_parse_from_format("j.n.Y",$data[DATUM]);
        $date_formated = $date['year'].'-'.sprintf("%02d", $date['month']).'-'.sprintf("%02d", $date['day']);

        $date_valute = date_parse_from_format("j.n.Y",$data[DATUM_ODEPSANI]);
        $date_valute_formated = $date_valute['year'].'-'.sprintf("%02d", $date_valute['month']).'-'.sprintf("%02d", $date_valute['day']);

        $out .= "<banka>";
        $out .= "<id>ext:RB-BANKA:".$data[KOD_TRANSAKCE]."</id>";
        $out .= "<typDokl>typDokl.zNastaveni</typDokl>";
        $out .= "<banka>code:" . $BankAccCode . "</banka>";
        $castka = preg_replace('/\s+/', '', $data[13]);
        $castka = str_replace(',', '.', $castka);
        $castkaF = floatval($castka);
        $out .= "<!--sumOsv>" . $castkaF . "</sumOsv-->";
        $out .= "<sumOsv>" . abs($castkaF) . "</sumOsv>";
        $out .= "<mena>code:".$data[SMENA]."</mena>";
        $out .= "<bezPolozek>true</bezPolozek>";
        if ($castkaF > 0.0) {
          $out .= "<typPohybuK>typPohybu.prijem</typPohybuK>";
	} else {
          $out .= "<typPohybuK>typPohybu.vydej</typPohybuK>";
        }
        $out .= "<datVyst>" . $date_formated . "</datVyst>";
        $out .= "<datUcto>" . $date_valute_formated . "</datUcto>";
                
        $out .= "<buc>".strstr($data[CISLO_UCTU],"/",true)."</buc>\n";                                // Parse part before slash: 190842040287
        $out .= "<smerKod>code:".substr($data[CISLO_UCTU],strpos($data[CISLO_UCTU], "/")+1)."</smerKod>\n";    // Parse part after slash: 0100

        $out .= "<konSym if-not-found=\"null\">code:".$data[KONSTANTNI_SYMBOL]."</konSym>\n";           // konstatní symbol, 10 znaků
        $out .= "<varSym>".$data[VARIABILNI_SYMBOL]."</varSym>\n";           // variabilní symbol, 10 znaků
        $out .= "<specSym>".$data[SPECIFICKY_SYMBOL]."</specSym>\n";           // specifický symbol, 10 znaků

        $out .= "<popis>".$data[ZPRAVA]."</popis>";
        $out .= "<poznam>".$data[POZNAMKA]."</poznam>";
        
        $out .= "</banka>";
    }
    $out  .= "</winstrom>\n";

    // Close input file
    fclose($handle);

    // Set output file (only when -x is not used)
    if (!array_key_exists("x", $options)) {
        if (array_key_exists("o", $options)) {
            // Use filename specifiled in command parameter -o
            $filename_output = $options["o"];
        } else {
            // Use default filename
            $filename_output = "output.xml";
        }
        // Handle file output
        if (@file_put_contents($filename_output,$out)) {
            // File written succesfully
            if (!array_key_exists("d", $options)) {
                // Write success info, but only when -d is not used
                echo "Succesfully converted into file: " . $filename_output . "\n\n";
            }
        } else {
            // File write failed
            echo "Error, cannot write to output file: " . $filename_input . "\n";
            echo "Use ./" . basename(__FILE__) . " -d for help\n\n";
            exit();
        }
    }

    // Handle display output
    if (array_key_exists("d", $options)) {
        // Display output IS turned on
        echo $out;
    }
}

?>
